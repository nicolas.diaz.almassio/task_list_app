class CreateTaskLists < ActiveRecord::Migration[6.1]
  def change
    create_table :task_lists do |t|
      t.integer :task_list_id
      t.string :name, null: false
      t.index :name, unique: true
      t.timestamps
    end
  end
end
