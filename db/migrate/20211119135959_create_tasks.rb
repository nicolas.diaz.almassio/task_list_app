class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :name
      t.boolean :completed, default: false
      t.integer :task_list_id
      t.index [:name, :task_list_id], unique: true
      t.timestamps
    end
    add_foreign_key :tasks, :task_lists
  end
end
