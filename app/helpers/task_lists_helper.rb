module TaskListsHelper
  def linked_name(task_list)
    link_to task_list.name, task_list_path(task_list)
  end
end