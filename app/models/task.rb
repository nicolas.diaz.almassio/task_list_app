class Task < ApplicationRecord
  belongs_to :task_list

  validates :name, presence: true,  uniqueness: { scope: :task_list_id }
  scope :completed, -> { where(completed: true) }
end
