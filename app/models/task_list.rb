class TaskList < ApplicationRecord
  has_many :tasks, dependent: :destroy

  validates :name, presence: true, uniqueness: true

  def completed_tasks
    tasks.completed.count
  end
end
