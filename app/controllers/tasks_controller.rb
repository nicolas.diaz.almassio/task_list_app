class TasksController < ApplicationController
  before_action :set_task, only: [:edit, :update, :destroy]

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_create_params)
    if @task.save
      flash[:notice] =  "Tarea creada exitosamente"
    else
      flash[:notice] = "Hubo algun problema al intentar crear la tarea"
    end
    redirect_back(fallback_location: root_path)
  end

  def edit
  end

  def update
    if @task.update(task_update_params)
      flash[:notice] = 'La tarea ha sido actualizada correctamente'
      redirect_back(fallback_location: root_path)
    else
      flash[:notice] = 'Hubo algun problema al actualizar la tarea'
      redirect_back(fallback_location: root_path)
    end
  end

  def destroy
    @task.destroy
    flash[:notice] = "La tarea ha sido eliminada correctamente"
    redirect_back(fallback_location: root_path)
  end

  private

  def task_create_params
    params.require(:task).permit(:name, :task_list_id)
  end

  def task_update_params
    params.require(:task).permit(:completed)
  end

  def set_task
    @task = Task.find(params[:id])
  end
end
