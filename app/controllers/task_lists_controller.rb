class TaskListsController < ApplicationController
  before_action :set_task_list, only: [:edit, :show, :update, :destroy]

  def index
    @task_lists = TaskList.all
    @task = Task.new
  end

  def new
    @task_list = TaskList.new
    render :form
  end

  def create
    @task_list = TaskList.new(task_list_params)
    if @task_list.save
      flash[:notice] =  "Lista de tareas creada exitosamente"
      redirect_to root_path
    else
      flash.now[:notice] = "Hubo algun problema al intentar crear la lista"
      render :form
    end
  end

  def show
    @task = Task.new
  end

  def edit
    render :form
  end

  def update
    if @task_list.update(task_list_params)
      redirect_to @task_list, notice: "La lista ha sido actualizada correctamente"
    else
      redirect_to @task_list, notice: "Hubo algun problema al actualizar la lista"
    end
  end

  def destroy
    @task_list.destroy
    flash[:notice] = "La lista ha sido eliminada correctamente"
    redirect_to root_path
  end

  private

  def task_list_params
    params.require(:task_list).permit(:name, :id)
  end

  def set_task_list
    @task_list = TaskList.find(params[:id])
  end
end
