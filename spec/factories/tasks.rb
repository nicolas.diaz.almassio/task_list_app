FactoryBot.define do
  factory :task do
    name { Faker::Lorem.sentence }
    association :task_list
  end
end
