FactoryBot.define do
  factory :task_list do
    name { Faker::Name.unique.name }

    factory :task_list_with_tasks do
      after(:create) do |task_list|
        create_list(:task, 3, task_list: task_list)
      end
    end

    factory :task_list_with_completed_tasks do
      after(:create) do |task_list|
        create_list(:task, 3, completed: true, task_list: task_list)
      end
    end
  end
end
