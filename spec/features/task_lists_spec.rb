require 'rails_helper'

RSpec.feature "TaskLists", type: :feature do
  describe "CRUD actions" do
    scenario "user creates a new task list" do
      when_i_visit_the_root_page
      when_i_click_on_create_a_new_task_list_button
      when_i_want_to_create_a_task_list
      then_the_page_should_have_the_created_task_list_name
    end

    scenario "user deletes a task list" do
      @list = create(:task_list)
      name = @list.name
      when_i_visit_the_root_page
      when_i_click_on_a_task_list_name(@list)
      when_i_click_on_delete_button
      then_the_page_should_not_have_the_task_list_name(name)
    end

    context "user edits a task list" do
      before do
        @list = create(:task_list)
      end

      scenario "with a valid new name" do
        when_i_visit_the_root_page
        when_i_click_on_a_task_list_name(@list)
        when_i_click_on_edit_button(@list)
        when_i_want_to_edit_a_task_list_with_valid_name(@list)
        then_the_page_should_have_the_list_new_name(@list)
      end

      scenario "with an invalid new name" do
        old_list = create(:task_list, name: "tarea vieja")
        when_i_visit_the_root_page
        when_i_click_on_a_task_list_name(@list)
        when_i_click_on_edit_button(@list)
        when_i_want_to_edit_a_task_list_with_invalid_name(@list)
        then_the_page_should_not_have_the_old_list_name(@list)
      end
    end
  end
  
  describe "Show the index page" do
    scenario "The lists are alphabetically ordered" do
      given_i_have_three_lists_with_names("Acampar", "Caminar", "Descansar")
      when_i_visit_the_root_page
      then_they_should_be_ordered_alphabetically
    end
  end

  describe "Show the task list page" do
    scenario "task list with tasks" do
      list = create(:task_list_with_tasks)
      when_i_visit_the_root_page
      when_i_click_on_a_task_list_name(list)
      when_i_visit_a_task_list_show_page(list)
    end

    scenario "task list without tasks" do
      list = create(:task_list)
      when_i_visit_the_root_page
      when_i_click_on_a_task_list_name(list)
      when_i_visit_a_task_list_show_page(list)
    end
  end

  private

  def when_i_visit_the_root_page
    visit root_path
    expect(page).to have_link("Nueva lista de tareas")
  end

  def when_i_click_on_create_a_new_task_list_button
    click_link "Nueva lista de tareas"
    expect(page).to have_field "task_list[name]"
    expect(page).to have_button "Guardar lista"
    expect(page).to have_link "Volver"
  end

  def when_i_want_to_create_a_task_list
    fill_in "task_list[name]", with: "primera lista"
    click_button "Guardar lista"
    expect(page).to have_content "Lista de tareas creada exitosamente"
  end

  def then_the_page_should_have_the_created_task_list_name
    expect(page).to have_content "primera lista"
  end

  def when_i_click_on_a_task_list_name(list)
    click_link "#{list.name}"
    expect(page).to have_content "#{list.name}"
    expect(page).to have_link "Volver"
    expect(page).to have_link "Editar"
    expect(page).to have_button "Eliminar"
  end

  def when_i_click_on_edit_button(list)
    click_link "Editar"
    expect(page).to have_content "#{list.name}"
    expect(page).to have_content "Nombre de la lista"
    expect(page).to have_field "task_list[name]"
    expect(page).to have_button "Guardar"
    expect(page).to have_link "Volver"
  end

  def when_i_want_to_edit_a_task_list_with_valid_name(list)
    fill_in "task_list[name]", with: "segunda lista"
    click_button "Guardar"
  end

  def then_the_page_should_have_the_list_new_name(list)
    expect(page).to have_content "La lista ha sido actualizada correctamente"
    expect(page).to have_content "segunda lista"
  end

  def when_i_want_to_edit_a_task_list_with_invalid_name(list)
    fill_in "task_list[name]", with: "tarea vieja"
    click_button "Guardar"
  end

  def then_the_page_should_not_have_the_old_list_name(list)
    expect(page).to have_content "Hubo algun problema al actualizar la lista"
    expect(page).not_to have_content "tarea vieja"
  end

  def when_i_click_on_delete_button
    click_button "Eliminar"
    expect(page).to have_content "La lista ha sido eliminada correctamente"
  end
  
  def then_the_page_should_not_have_the_task_list_name(name)
    expect(page).not_to have_content name
  end

  def when_i_visit_a_task_list_show_page(list)
    expect(page).to have_content "#{list.name}"
    expect(page).to have_link "Volver"
    expect(page).to have_link "Editar"
    expect(page).to have_button "Eliminar"
    if list.tasks.any?
      list.tasks.each do |task|
        expect(page).to have_content "#{task.name}"
      end
    end
  end

  def given_i_have_three_lists_with_names(list1, list2, list3)
    @list1 = create(:task_list, name: list1)
    @list2 = create(:task_list, name: list2)
    @list3 = create(:task_list, name: list3)
  end

  def then_they_should_be_ordered_alphabetically
    expect(page.text.index("Acampar")).to be < page.text.index("Caminar")
    expect(page.text.index("Caminar")).to be < page.text.index("Descansar")
  end
end