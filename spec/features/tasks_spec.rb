require 'rails_helper'

RSpec.feature "Tasks", type: :feature do
  describe "user creates a task whitin a task list" do
    before do
      @list = create(:task_list)
    end

    scenario "from the index page" do
      when_i_visit_the_root_page
      when_i_want_to_create_a_task
      then_the_page_should_have_the_task_name
    end

    scenario "from the task list show page" do
      when_i_visit_the_root_page
      when_i_click_on_a_task_list_name(@list)
      when_i_want_to_create_a_task
      then_the_page_should_have_the_task_name
    end
  end

  describe "user completes a task whitin a task list" do
    before do
      @list = create(:task_list_with_tasks)
    end

    scenario "from the index page" do
      when_i_visit_the_root_page
      when_i_click_on_the_complete_button_in_the_index_page(@list.tasks.first)
      then_the_page_should_have_the_completed_tasks_count(@list)
    end

    scenario "from the task list show page" do
      when_i_visit_the_root_page
      when_i_click_on_a_task_list_name(@list)
      when_i_click_on_the_complete_button_in_the_task_list_show_page(@list.tasks.first)
      then_the_completed_task_should_be_below_the_completed_tasks_subtitle(@list.tasks.first)
    end
  end

  describe "user deletes a task whitin a task list" do
    before do
      @list = create(:task_list_with_tasks)
    end

    scenario "from the index page" do
      when_i_visit_the_root_page
      when_i_click_on_the_delete_task_button(@list.tasks.first)
      then_the_task_list_should_have_2_tasks(@list)
    end

    scenario "from the task list show page" do
      when_i_visit_the_root_page
      when_i_click_on_a_task_list_name(@list)
      when_i_click_on_the_delete_task_button(@list.tasks.first)
      then_the_task_list_should_have_2_tasks(@list)
    end
  end

  private

  def when_i_visit_the_root_page
    visit root_path
    expect(page).to have_link("Nueva lista de tareas")
  end

  def when_i_click_on_a_task_list_name(list)
    click_link "#{list.name}"
    expect(page).to have_content "#{list.name}"
    expect(page).to have_link "Volver"
    expect(page).to have_link "Editar"
    expect(page).to have_button "Eliminar"
  end

  def when_i_want_to_create_a_task
    fill_in "Nueva tarea...", with: "primera tarea"
    click_button "Crear"
  end

  def then_the_page_should_have_the_task_name
    expect(page).to have_content "Tarea creada exitosamente"
    expect(page).to have_content "primera tarea"
  end

  def when_i_click_on_the_complete_button_in_the_index_page(task)
    click_button("complete-#{task.name}")
    expect(page).not_to have_content "#{task.name}"
  end

  def then_the_page_should_have_the_completed_tasks_count(list)
    expect(page).to have_content "1 tarea completada"
  end

  def when_i_click_on_the_complete_button_in_the_task_list_show_page(task)
    click_button("complete-#{task.name}")
  end

  def then_the_completed_task_should_be_below_the_completed_tasks_subtitle(task)
    expect(page.text.index("Tareas Completadas")).to be < page.text.index("#{task.name}")
  end

  def when_i_click_on_the_delete_task_button(task)
    click_button("delete-#{task.name}")
    expect(page).not_to have_content "#{task.name}"
  end

  def then_the_task_list_should_have_2_tasks(list)
    expect(list.tasks.count).to eq 2
  end
end