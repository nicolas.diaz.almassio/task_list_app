require 'rails_helper'

RSpec.describe TaskList, type: :model do
  
  describe 'Basics' do
    it 'has a valid factory' do
      expect(build(:task_list)).to be_valid
    end
  end

  describe 'Associations' do
    it { is_expected.to have_many(:tasks).dependent(:destroy)}
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:name) }

    context 'uniqueness validations' do
      subject { create(:task_list) }

      it { is_expected.to validate_uniqueness_of(:name) }
    end
  end

  describe '.completed_tasks' do
    subject { create(:task_list_with_completed_tasks) }

    it 'returns the quantitiy of completed tasks' do
      expect(subject.completed_tasks).to eq 3
    end
  end
end