require 'rails_helper'

RSpec.describe Task, type: :model do

  describe 'Basics' do
    it 'has a valid factory' do
      expect(build(:task)).to be_valid
    end
  end

  describe 'Associations' do
    it { is_expected.to belong_to(:task_list)}
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:name) }

    context 'uniqueness validations' do
      subject { create(:task) }

      it { is_expected.to validate_uniqueness_of(:name).scoped_to(:task_list_id) }
    end
  end
end
