require 'rails_helper'

RSpec.describe TaskListsController, type: :controller do  

  describe "Get index" do
    before do
      create(:task_list)
    end
    
    it "has a successfull response" do
      get :index
      expect(response).to be_successful
    end
  end

  describe "Get show" do
    before do
      @list = create(:task_list_with_tasks)
    end

    it "has a successfull response" do
      get :show, params: { id: @list.id }
      expect(response).to be_successful
    end
  end

  describe "Get new" do
    it "renders the new template" do
      get :new
      expect(response).to be_successful
    end
  end

  describe "Post to create" do
    it "creates a new list" do
      expect { 
        post :create, params: { task_list: { name: "primera lista" } }
      }.to change { TaskList.count }.by(1)

      expect(TaskList.last.name).to eq("primera lista")
    end
  end

  describe "Get edit" do
    before do
      @list = create(:task_list)
    end

    it "renders the new template" do
      get :edit, params: { id: @list.id }
      expect(response).to be_successful
    end
  end

  describe "Patch to update" do
    before do
      @list = create(:task_list)
    end

    it "updates the existing list" do
      expect {
        patch :update, params: { task_list: { name: 'nombre actualizado' }, id: @list.id }
      }.to change { @list.reload.name }
       .to('nombre actualizado')
    end
  end

  describe "delete to destroy" do
    before do
      @list = create(:task_list)
    end

    it "deletes the list" do
      expect do
        delete :destroy, params: { id: @list.id }
      end.to change(TaskList, :count).by(-1)
    end
  end
end