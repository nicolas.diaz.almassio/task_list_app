require 'rails_helper'

RSpec.describe TasksController, type: :controller do

  describe "Post to create" do
    before do
      @list = create(:task_list)
    end

    it "creates a new task" do
      expect do
        post :create, params: { task: { name: "primera tarea", task_list_id: @list.id } }
      end.to change(@list.tasks, :count).by(1)
    end
  end

  describe "Patch to update" do
    before do
      @list = create(:task_list_with_tasks)
    end

    it "updates the existing list" do
      expect {
        patch :update, params: { task: { completed: true }, id: @list.tasks.last.id }
      }.to change { @list.tasks.last.completed }
       .to(true)
    end
  end

  describe "Post to destroy" do
    before do
      @list = create(:task_list_with_tasks)
    end

    it "deletes a task" do
      expect do
        delete :destroy, params: { id: @list.tasks.last.id }
      end.to change(@list.tasks, :count).by(-1)
    end
  end
end